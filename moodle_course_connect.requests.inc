<?php

/**
 * @file
 * Fetcher for the moodle course connect module.
 */


function moodle_course_connect_request($function) {
  static $client = NULL;
	$domain = variable_get('mdl_domain', '');
	$token = variable_get('mdl_courseconnect_token', '');

  if (!$client) {
    $wsdl = "{$domain}/webservice/soap/server.php?wsdl=1&wstoken={$token}";
		$client = new SoapClient($wsdl, array(
			'cache_wsdl' => WSDL_CACHE_NONE,
		));
  }

  try {
    $args = func_get_args();
    array_shift($args);

    $response = call_user_func_array(array($client, $function), ($args));

  } catch (Exception $e) {
    
    drupal_set_message(t('Moodle API error: %e', array('%e' => $e->getMessage())), 'error');
    drupal_set_message(t('Moodle API error detail: %e', array('%e' => $e->detail)), 'error');
    return FALSE;
  }

  return $response;
}
