<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Web service local plugin template external functions and service definitions.
 *
 * @package    localwstemplate
 * @copyright  2011 Jerome Mouneyrac
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// We defined the web service functions to install.
$functions = array(
        'local_courseconnect_get_course_progress' => array(
            'classname'   => 'local_courseconnect_external',
            'methodname'  => 'get_course_progress',
            'classpath'   => 'local/courseconnect/externallib.php',
            'description' => 'Return Hello World FIRSTNAME. Can change the text (Hello World) sending a new text as parameter',
            'type'        => 'read',
        ),
        'local_courseconnect_get_available_certificates' => array(
            'classname'   => 'local_courseconnect_external',
            'methodname'  => 'get_available_certificates',
            'classpath'   => 'local/courseconnect/externallib.php',
            'description' => 'Return Hello World FIRSTNAME. Can change the text (Hello World) sending a new text as parameter',
            'type'        => 'read',
        ),
        'local_courseconnect_content_is_availabe' => array(
            'classname'   => 'local_courseconnect_external',
            'methodname'  => 'content_is_availabe',
            'classpath'   => 'local/courseconnect/externallib.php',
            'description' => 'Return Hello World FIRSTNAME. Can change the text (Hello World) sending a new text as parameter',
            'type'        => 'read',
        ),
        'local_courseconnect_get_courses' => array(
            'classname'   => 'local_courseconnect_external',
            'methodname'  => 'get_courses',
            'classpath'   => 'local/courseconnect/externallib.php',
            'description' => 'Return Hello World FIRSTNAME. Can change the text (Hello World) sending a new text as parameter',
            'type'        => 'read',
        ),
);

// We define the services to install as pre-build services. A pre-build service is not editable by administrator.
$services = array(
        'Course connect service' => array(
            'functions' => array ('local_courseconnect_get_course_progress', 'local_courseconnect_get_available_certificates', 'core_course_get_courses', 'local_courseconnect_content_is_availabe', 'local_courseconnect_get_courses', 'core_course_get_contents', 'core_user_get_users_by_field', 'core_user_get_users', 'core_enrol_get_users_courses', 'core_user_get_course_user_profiles', 'core_enrol_get_users_courses', 'enrol_manual_enrol_users'),
            'restrictedusers' => 0,
            'enabled' => 1,
        )
);
