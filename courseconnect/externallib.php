<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * External Web Service Template
 *
 * @package    localwstemplate
 * @copyright  2011 Moodle Pty Ltd (http://moodle.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once($CFG->libdir . "/externallib.php");

class local_courseconnect_external extends external_api {

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function get_courses_parameters() {
        return new external_function_parameters(
                array(
                    'courseids' => new external_multiple_structure(
                        new external_value(PARAM_INT, 'Course id'),
                        '',
                        VALUE_DEFAULT, array()),
                    'ceu' => new external_value(PARAM_BOOL, '', VALUE_DEFAULT, NULL),
                    'uidnumber' => new external_value(PARAM_INT, '', VALUE_DEFAULT, 0)
                )
        );
    }

		//Used for comparing course objects
		private function customCallback($a, $b) {
				return $a->id - $b->id;
		}

    /**
     * Returns welcome message
     * @return string welcome message
     */
    public static function get_courses($courseids = array(), $ceu = NULL, $uidnumber = 0) {
        global $CFG, $DB;
        require_once($CFG->libdir . '/enrollib.php');
        require_once($CFG->libdir . '/modinfolib.php');
        require_once("{$CFG->libdir}/completionlib.php");
        require_once($CFG->dirroot.'/mod/scorm/locallib.php');
        
        $params = self::validate_parameters(self::get_courses_parameters(), array('courseids' => $courseids, 'ceu' => $ceu, 'uidnumber' => $uidnumber));

        //retrieve courses
        $userid = 0;
        $courses = array();
        $my_obj = new local_courseconnect_external();

				$sql = "SELECT course.* FROM {course} course ";
				$where = '';
				$query_params = array();
				//Check course type required by API
				if (isset($params['ceu'])) {
					if ($params['ceu']) {
						//Fetch all CEU courses
						$sql .= 'LEFT JOIN {course_info_data} cd ON cd.courseid = course.id
						LEFT JOIN {course_info_field} cf ON cd.fieldid = cf.id';
						$where .= 'cf.shortname = ? AND cd.data = ?';
						$query_params[] ='ceu';
						$query_params[] = 1;
					}
					else { 
						//Fetch all Non-CEU courses 
						$sql .= 'LEFT JOIN {course_info_data} cd ON cd.courseid = course.id
						LEFT JOIN {course_info_field} cf ON cd.fieldid = cf.id';
						$where .= 'cf.shortname = ? AND (cd.data IS NULL OR cd.data=0)';
            $query_params[] ='ceu';						
					}					
				}
				else {
					//If not set, then return both CEU or Non-CEU. Nothing to do here
				}
				if (isset($params['courseids'])) {
					//default course id
					$cids = "(0)";
					if (count($params['courseids']) > 0) {
						$injected = false;
						foreach($params['courseids'] as $course_id) {
							if (!is_numeric($course_id)) {
								//atleast a value is not Int, suspecious, do not have IN query at all and return empty courses.
								$injected = true;
								break;
								
							}
						}
						//only if query is safe, limit courses
						if (!$injected) {
							$cids = "("  . implode(",", $params['courseids']) . ")";
						}
					  if (!empty($where)) {
						  $where .= " AND";
					  }
					  $where .= " course.id IN  {$cids}";
				  }
				}
				if (!empty($where)) {
					$where =  " WHERE course.format != 'site' AND ". $where;
				}
        else {
          $where =  " WHERE course.format != 'site'";
        }
        
				$sql = $sql . $where;
				$courses = $DB->get_records_sql($sql, $query_params);

				//If user id passed, then return just his courses
        if (isset($params['uidnumber'])) {
						$userid = $params['uidnumber'] ? $DB->get_field('user', 'id', array('idnumber' => $params['uidnumber']), MUST_EXIST) : 0;
						$user_results = enrol_get_users_courses($userid, true, 'id, fullname, shortname, category, format, startdate, visible');
						$courses = array_uintersect($user_results, $courses, array($my_obj, 'customCallback'));
        }

        //create return value
        $coursesinfo = array();
        foreach ($courses as $course) {
            $courseinfo = array();
            $courseinfo['id'] = $course->id;
            $courseinfo['fullname'] = $course->fullname;
            $courseinfo['shortname'] = $course->shortname;
            $courseinfo['categoryid'] = $course->category;
            $courseinfo['format'] = $course->format;
            $courseinfo['startdate'] = $course->startdate;
            $courseinfo['visible'] = $course->visible;

						if ($userid) {
							$cinfo = new completion_info($course);
							$courseinfo['completed'] = $cinfo->is_course_complete($userid);
							$params = array(
                'userid' => $userid,
								'course' => $course->id
							);
							$ccompletion = new completion_completion($params);
							$courseinfo['started'] = $ccompletion->timestarted ? $ccompletion->timestarted : 0;
						}

            $coursecontents = array();
            $modinfo = get_fast_modinfo($course->id, $userid);
            $sections = $modinfo->get_section_info_all();
            $modinfosections = $modinfo->get_sections();
            foreach ($sections as $key => $section) {
                if (!$section->uservisible) {
                    continue;
                }
                $sectionvalues = array();
                $sectionvalues['id'] = $section->id;
                $sectionvalues['name'] = get_section_name($course, $section);
                $sectionvalues['visible'] = $section->visible;
                $sectioncontents = array();
                if (!empty($modinfosections[$section->section])) {
                    foreach ($modinfosections[$section->section] as $cmid) {
                        $cm = $modinfo->cms[$cmid];
                        if (!$cm->uservisible) {
                            continue;
                        }
                        $module = array();
                        $module['id'] = $cm->id;
                        $module['name'] = format_string($cm->name, true);
                        $module['modname'] = $cm->modname;
                        $url = $cm->url;
                        if ($url) { //labels don't have url
                            $module['url'] = $url->out(false);
                        }
                        if ($cm->modname == 'scorm') {
													$scorm = $DB->get_record("scorm", array("id"=>$cm->instance));
													$module['scorm_id'] = $scorm->id;
													$sco = scorm_get_sco($scorm->launch, SCO_ONLY);
													$module['scoid'] = $sco->id;
												}
                        $module['visible'] = $cm->visible;
                        $module['available'] = $cm->available;
                        $sectioncontents[] = $module;
                    }
                }
                $sectionvalues['modules'] = $sectioncontents;
                $coursecontents[] = $sectionvalues;
            }

            $courseinfo['contents'] = $coursecontents;
            $coursesinfo[] = $courseinfo;
        }
        
        return $coursesinfo;
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function get_courses_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'course id'),
                    'fullname' => new external_value(PARAM_TEXT, 'course full name'),
                    'shortname' => new external_value(PARAM_TEXT, 'course short name'),
                    'categoryid' => new external_value(PARAM_INT, 'category id'),
                    'format' => new external_value(PARAM_PLUGIN, 'course format: weeks, topics, social, site,..'),
                    'startdate' => new external_value(PARAM_INT, 'timestamp when the course start'),
                    'visible' => new external_value(PARAM_INT, '1: available to student, 0:not available', VALUE_OPTIONAL),
                    'completed' => new external_value(PARAM_BOOL, 'course completed or not', VALUE_OPTIONAL),
                    'started' => new external_value(PARAM_INT, 'course not yet started', VALUE_OPTIONAL),
                    'contents' => new external_multiple_structure(
                        new external_single_structure(
                            array(
                                'id' => new external_value(PARAM_INT, 'Section ID'),
                                'name' => new external_value(PARAM_TEXT, 'Section name'),
                                'visible' => new external_value(PARAM_INT, 'is the section visible', VALUE_OPTIONAL),
                                'modules' => new external_multiple_structure(
                                        new external_single_structure(
                                            array(
                                                'id' => new external_value(PARAM_INT, 'activity id'),
                                                'scorm_id' => new external_value(PARAM_INT, 'scorm id', VALUE_OPTIONAL),
                                                'scoid' => new external_value(PARAM_INT, 'sco id', VALUE_OPTIONAL),
                                                'url' => new external_value(PARAM_URL, 'activity url', VALUE_OPTIONAL),
                                                'name' => new external_value(PARAM_RAW, 'activity module name'),
                                                'visible' => new external_value(PARAM_INT, 'is the module visible', VALUE_OPTIONAL),
                                                'available' => new external_value(PARAM_BOOL, 'is the module available', VALUE_OPTIONAL),
                                                'modname' => new external_value(PARAM_PLUGIN, 'activity module type'),
                                            )
                                        ), 'list of module'
                                )
                            )
                        )
                    ),
                ), 'course'
            )
        );
    }


}
